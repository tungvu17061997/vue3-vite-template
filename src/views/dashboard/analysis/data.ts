export interface GrowCardItem {
  icon: string;
  title: string;
  value: number;
  value2?: number;
  suffix_value?: string;
  suffix_value2?: string;
  total?: number;
  color: string;
  action: string;
}
