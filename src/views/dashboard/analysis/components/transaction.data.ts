import { BasicColumn, FormSchema } from '/@/components/Table';

export const columns: BasicColumn[] = [
  {
    title: 'Mã GD',
    dataIndex: 'transid',
    align: 'center',
    width: 200
  },
  {
    title: 'Mã đơn hàng DN',
    dataIndex: 'order_id',
    align: 'center',
    width: 200
  },
  {
    title: 'Tên doanh nghiệp',
    dataIndex: 'merchant_name',
    align: 'center',
    width: 150
  },
  {
    // show in table
    title: 'Cửa hàng',
    align: 'center',
    dataIndex: 'store_label',
    children: [
      {
        title: 'Tên cửa hàng',
        dataIndex: 'store_label',
        width: 160,
        align: 'center'
      },
      {
        title: 'Hình thức thanh toán',
        dataIndex: 'payment_type',
        width: 160,
        align: 'center',
        slots: { customRender: 'payment_type' }
      },
      {
        title: 'Tài khoản thanh toán',
        dataIndex: 'type',
        width: 160,
        align: 'center',
        slots: { customRender: 'type' }
      }
    ]
  },
  {
    title: 'Số tiền giao dịch',
    dataIndex: 'amount',
    align: 'right',
    width: 250
  },
  {
    title: 'Phí giao dịch',
    dataIndex: 'total_fee',
    align: 'right',
    width: 250
  },
  {
    title: 'Số tiền hoàn',
    dataIndex: 'refunded_amt',
    align: 'right',
    width: 250
  },
  {
    title: 'Create time',
    dataIndex: 'createdAt',
    width: 180,
    slots: { customRender: 'createdAt' }
  },
  {
    title: 'Trạng thái',
    dataIndex: 'status',
    align: 'center',
    width: 150
  }
];
