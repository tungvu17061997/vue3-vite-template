import { getAllRoleList } from '/@/api/demo/system';
import { BasicColumn, FormSchema } from '/@/components/Table';

export const columns: BasicColumn[] = [
  {
    title: 'username',
    dataIndex: 'account',
    width: 120
  },
  {
    title: 'nickname',
    dataIndex: 'nickname',
    width: 120
  },
  {
    title: 'mailbox',
    dataIndex: 'email',
    width: 120
  },
  {
    title: 'Create time',
    dataIndex: 'createTime',
    width: 180
  },
  {
    title: 'Character',
    dataIndex: 'role',
    width: 200
  },
  {
    title: 'Note',
    dataIndex: 'remark'
  }
];

export const searchFormSchema: FormSchema[] = [
  {
    field: 'account',
    label: 'username',
    component: 'Input',
    colProps: { span: 8 }
  },
  {
    field: 'nickname',
    label: 'nickname',
    component: 'Input',
    colProps: { span: 8 }
  }
];

export const accountFormSchema: FormSchema[] = [
  {
    field: 'account',
    label: 'username',
    component: 'Input',
    required: true
  },
  {
    field: 'pwd',
    label: 'password',
    component: 'InputPassword',
    required: true,
    ifShow: false
  },
  {
    label: 'Character',
    field: 'role',
    component: 'ApiSelect',
    componentProps: {
      api: getAllRoleList,
      labelField: 'roleName',
      valueField: 'roleValue'
    },
    required: true
  },
  {
    field: 'dept',
    label: 'Department',
    component: 'TreeSelect',
    componentProps: {
      replaceFields: {
        title: 'deputy',
        key: 'id',
        value: 'id'
      },
      getPopupContainer: () => document.body
    },
    required: true
  },
  {
    field: 'nickname',
    label: 'nickname',
    component: 'Input',
    required: true
  },

  {
    label: 'mailbox',
    field: 'email',
    component: 'Input',
    required: true
  },

  {
    label: 'Note',
    field: 'remark',
    component: 'InputTextArea'
  }
];
