import { t } from '/@/hooks/web/useI18n';
import { LAYOUT } from '/@/router/constant';
import type { AppRouteModule } from '/@/router/types';

const setting: AppRouteModule = {
  path: '/setting',
  name: 'Setting',
  component: LAYOUT,
  redirect: '/setting/index',
  meta: {
    roles: ['*'],
    hideChildrenInMenu: true,
    icon: 'ant-design:setting-outlined',
    title: t('routes.dashboard.setting'),
    orderNo: 14
  },
  children: [
    {
      path: 'index',
      name: 'SettingPage',
      component: () => import('/@/views/setting/index.vue'),
      meta: {
        title: t('routes.dashboard.about'),
        icon: 'ant-design:setting-outlined'
        // hideMenu: true,
      }
    }
  ]
};

export default setting;
