import { t } from '/@/hooks/web/useI18n';
import { LAYOUT } from '/@/router/constant';
import type { AppRouteModule } from '/@/router/types';

const support: AppRouteModule = {
  path: '/support',
  name: 'Support',
  component: LAYOUT,
  redirect: '/support/list',
  meta: {
    roles: ['*', 'support'],
    hideChildrenInMenu: true,
    orderNo: 13,
    icon: 'fluent-person-support-20-regular',
    title: t('routes.dashboard.support')
  },
  children: [
    {
      path: 'list',
      name: 'supportList',
      component: () => import('/@/views/support/list/index.vue'),
      meta: {
        affix: true,
        title: t('routes.dashboard.support')
      }
    },
    {
      path: '/support/detail/:id',
      name: 'supportDetail',
      meta: {
        hideMenu: true,
        title: t('routes.dashboard.support-detail'),
        ignoreKeepAlive: true,
        showMenu: false,
        currentActiveMenu: '/support'
      },
      component: () => import('/@/views/support/detail/SupportDetail.vue')
    }
  ]
};

export default support;
