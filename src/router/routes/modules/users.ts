import { t } from '/@/hooks/web/useI18n';
import { LAYOUT } from '/@/router/constant';
import type { AppRouteModule } from '/@/router/types';

const users: AppRouteModule = {
  path: '/user',
  name: 'Accounts',
  component: LAYOUT,
  redirect: '/user/list',
  meta: {
    roles: ['*', 'user'],
    hideChildrenInMenu: true,
    orderNo: 11,
    icon: 'mi-users',
    title: t('routes.dashboard.user')
  },
  children: [
    {
      path: 'list',
      name: 'List',
      component: () => import('/@/views/account/list/index.vue'),
      meta: {
        affix: true,
        title: t('routes.dashboard.user')
      }
    },
    {
      path: '/user/detail/:id',
      name: 'detail',
      meta: {
        hideMenu: true,
        title: t('routes.dashboard.user-detail'),
        ignoreKeepAlive: true,
        showMenu: false,
        currentActiveMenu: '/user'
      },
      component: () => import('/@/views/account/detail/AccountDetail.vue')
    }
  ]
};

export default users;
