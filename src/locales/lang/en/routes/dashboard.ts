export default {
  dashboard: 'Dashboard',
  about: 'About',
  setting: 'Setting',
  support: 'Support',
  'support-detail': 'Support Detail',
  workbench: 'Workbench',
  analysis: 'Analysis',
  transactions: 'Transactions',
  'transaction-detail': 'Transaction Detail',
  user: 'Users',
  'user-detail': 'User Detail'
};
