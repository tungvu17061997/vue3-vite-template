import { defHttp } from '/@/utils/http/axios';

export const adminApi = {
  getStatistics(from_time: number, to_time: number) {
    return defHttp.get({ url: 'transaction/statistic-total', params: { from_time, to_time } });
  },
  queryTransactions({perPage, currentPage}) {
    return defHttp.get({
      url: 'transaction/list',
      params: { perPage, currentPage,  populate: 'store',} ,
    });
  },
  currentUser() {
    return defHttp.get({ url: 'auth/profile' });
  }
  // getSupport({ perPage, currentPage }) {
  //   return defHttp.get({ url: '/support/admin', params: { perPage, currentPage } });
  // },
};
