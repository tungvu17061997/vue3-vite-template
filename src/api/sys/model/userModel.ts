/**
 * @description: Login interface parameters
 */
export interface LoginParams {
  phone: string | number;
  password: string;
}

export interface RoleInfo {
  roleName: string;
  value: string;
}

export interface LoginResultModel {
  [key: string]: any;
  token: { accessToken: string; expiresIn: number };
}

export interface GetUserInfoModel {
  [key: string]: any;
}
