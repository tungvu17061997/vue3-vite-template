import { defHttp } from '/@/utils/http/axios';

export const settingApi = {
  getSetting() {
    return defHttp.get({ url: '/setting/' });
  },
  updateSetting(data) {
    return defHttp.put({ url: '/setting/', data });
  }
};
