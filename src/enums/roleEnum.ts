export enum RoleEnum {
  // super admin
  '*' = '*',
  'SUPPORT' = 'support',
  'USER' = 'user',

  // tester
  TEST = 'test'
}
